import data.HouseWithRule;
import data.Student;
import datamapper.DataMapper;
import filereader.FileReader;
import filereader.FileReaderImpl;
import sorter.Sorter;
import sorter.SorterImpl;
import printer.ConsolePrinterImpl;
import printer.Printer;

import java.util.List;

public class HarrySortingHatRunner {

    private static final String RULES_FILE_URL = "src/main/resources/files/rules.txt";
    private static final String STUDENT_FILE_URL = "src/main/resources/files/students.txt";

    private final FileReader fileReader;
    private final Sorter sorter;

    public HarrySortingHatRunner() {
        Printer printer = new ConsolePrinterImpl();
        fileReader = new FileReaderImpl(printer);
        sorter = new SorterImpl(printer);
    }

    public void execute() {
        List<HouseWithRule> housesWithRule = DataMapper.getHousesWithRule(fileReader.getElementsByUrl(RULES_FILE_URL));

        List<Student> students = DataMapper.getStudents(fileReader.getElementsByUrl(STUDENT_FILE_URL));

        sorter.sort(housesWithRule, students);
    }
}
