package exceptions;

public class HarrySortingHatExceptions extends RuntimeException{

    public HarrySortingHatExceptions(String message) {
        super(message);
    }
}
