package sorter;

import data.HouseWithRule;
import data.Student;

import java.util.List;

public interface Sorter {

    void sort(List<HouseWithRule> housesWithRule, List<Student> students);
}
