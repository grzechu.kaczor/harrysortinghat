package sorter;


import data.HouseWithRule;
import data.Rules;
import data.Student;
import datamapper.DataMapper;
import exceptions.HarrySortingHatExceptions;
import printer.Printer;

import java.util.*;

public class SorterImpl implements Sorter {

    private final Map<HouseWithRule, List<Student>> houses = new HashMap<>();

    Printer printer;

    public SorterImpl(Printer printer) {
        this.printer = printer;
    }

    public void sort(List<HouseWithRule> housesWithRule, List<Student> students) {
        sortStudentsToHousesByRole(housesWithRule, students);

        printer.print(DataMapper.getSimpleHousesWithStudents(houses));
    }

    private void sortStudentsToHousesByRole(List<HouseWithRule> housesWithRule, List<Student> students) {
        students.forEach(student -> {
            for (Rules rule : Rules.values()) {
                if (rule.matchStudentToRule(student)) {
                    addStudentsToHouse(getHouseByRule(housesWithRule, rule), student);
                    break;
                }
            }
        });
    }

    private HouseWithRule getHouseByRule(List<HouseWithRule> housesWithRule, Rules rule) {
        return housesWithRule.stream()
                .filter(x -> x.getRule().equalsIgnoreCase(rule.name))
                .findFirst()
                .orElseThrow(() -> new HarrySortingHatExceptions(String.format("There aren't a house with %s", rule.name)));
    }

    private void addStudentsToHouse(HouseWithRule houseWithRule, Student student) {
        if (!houses.containsKey(houseWithRule)) {
            houses.put(houseWithRule, new ArrayList<>());
        }
        List<Student> studentsInHouse = houses.get(houseWithRule);
        studentsInHouse.add(student);
    }
}
