package filereader;

import java.util.List;

public interface FileReader {

    List<String> getElementsByUrl(String url);

}
