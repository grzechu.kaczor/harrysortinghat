package filereader;

import exceptions.HarrySortingHatExceptions;
import printer.Printer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileReaderImpl implements FileReader {

    Printer printer;

    public FileReaderImpl(Printer printer) {
        this.printer = printer;
    }

    public List<String> getElementsByUrl(String url) {
        Path path = Paths.get(url);
        try {
            List<String> lines = Files.readAllLines(path);
            printer.print(lines);
            return lines;
        } catch (IOException e) {
            String errorMessage = String.format("There is a problem with read or find file from %s", url);
            throw new HarrySortingHatExceptions(errorMessage);
        }
    }
}
