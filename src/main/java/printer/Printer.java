package printer;

import java.util.List;
import java.util.Map;

public interface Printer {

    void print(List<String> element);

    void print(Map<String, List<String>> housesWithStudents);

}
