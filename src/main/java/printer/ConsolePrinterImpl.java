package printer;

import java.util.List;
import java.util.Map;

public class ConsolePrinterImpl implements Printer {

    private static final String CONNECTOR_ELEMENTS = " | ";
    private static final String CONNECTOR_STUDENTS = ", ";

    public void print(List<String> elements) {
        String readyLine = String.join(CONNECTOR_ELEMENTS, elements);
        System.out.println(readyLine);
    }

    public void print(Map<String, List<String>> housesWithStudents) {
        printEmptyLine();
        printEmptyLine();
        printStudentsByHouses(housesWithStudents);
    }

    private void printStudentsByHouses(Map<String, List<String>> housesWithStudents){
        for (Map.Entry<String, List<String>> entry : housesWithStudents.entrySet()) {
            String joinedStudents = String.join(CONNECTOR_STUDENTS, entry.getValue());
            String readyLine = String.format("%s : %s", entry.getKey(), joinedStudents);
            System.out.println(readyLine);
        }
    }

    private void printEmptyLine() {
        System.out.println();
    }
}
