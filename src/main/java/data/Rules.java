package data;

import java.util.Locale;

public enum Rules {

    LENGTH("length"){
        @Override
        public boolean matchStudentToRule(Student student) {
           return student.getFullName().length() > 20;
        }
    },
    VOWEL("vowel") {
        @Override
        public boolean matchStudentToRule(Student student) {
            return student.getFullName().toLowerCase(Locale.ROOT).matches("[aeiouy]");
        }
    },
    EVEN("even") {
        @Override
        public boolean matchStudentToRule(Student student) {
            return student.getFullName().length() % 2 == 0;
        }
    },
    DEFAULT("default") {
        @Override
        public boolean matchStudentToRule(Student student) {
            return true;
        }
    };

    public final String name;

    Rules(String name) {
        this.name = name;
    }

    public abstract boolean matchStudentToRule(Student student);

}
