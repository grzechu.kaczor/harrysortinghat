package data;

public class HouseWithRule {

    private final String houseName;
    private final String rule;

    public HouseWithRule(String houseName, String rule) {
        this.houseName = houseName;
        this.rule = rule;
    }

    public String getHouseName() {
        return houseName;
    }

    public String getRule() {
        return rule;
    }
}
