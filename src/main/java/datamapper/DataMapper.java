package datamapper;

import data.HouseWithRule;
import data.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataMapper {

    public static List<HouseWithRule> getHousesWithRule(List<String> rulesLines) {
        return rulesLines.stream()
                .map(line -> line.split(":"))
                .map(x -> new HouseWithRule(x[0].trim(), x[1].trim()))
                .collect(Collectors.toList());
    }

    public static List<Student> getStudents(List<String> studentsLines) {
        return studentsLines.stream()
                .map(x -> new Student(x.trim()))
                .collect(Collectors.toList());
    }

    public static Map<String, List<String>> getSimpleHousesWithStudents(Map<HouseWithRule, List<Student>> housesWithStudents) {
        Map<String, List<String>> map = new HashMap<>();
        for (Map.Entry<HouseWithRule, List<Student>> entry : housesWithStudents.entrySet()) {
            map.put(entry.getKey().getHouseName(), getFullNamesFromStudents(entry.getValue()));
        }
        return map;
    }

    private static List<String> getFullNamesFromStudents(List<Student> students) {
        return students.stream()
                .map(Student::getFullName)
                .collect(Collectors.toList());
    }
}
