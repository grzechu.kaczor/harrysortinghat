package sorter;

import data.HouseWithRule;
import data.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import printer.ConsolePrinterImpl;
import printer.Printer;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SorterImplTest {

    private Sorter sorter;

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp() {
        Printer printer = new ConsolePrinterImpl();
        sorter = new SorterImpl(printer);
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void shouldPrintCorrectStudentsByHouses() {
        // given
        List<HouseWithRule> housesWithRule = getHousesWithRule();
        List<Student> students = getStudents();
        String expect = getExpectedOutput();
        // when
        sorter.sort(housesWithRule, students);
        // then
        assertEquals(expect, outputStreamCaptor.toString().trim());
    }

    private List<HouseWithRule> getHousesWithRule() {
        List<HouseWithRule> housesWithRule = new ArrayList<>();
        HouseWithRule house1 = new HouseWithRule("Vowel House", "vowel");
        housesWithRule.add(house1);
        HouseWithRule house2 = new HouseWithRule("Long House", "length");
        housesWithRule.add(house2);
        HouseWithRule house3 = new HouseWithRule("Even House", "even");
        housesWithRule.add(house3);
        HouseWithRule house4 = new HouseWithRule("Default House", "default");
        housesWithRule.add(house4);
        return housesWithRule;
    }

    private List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        Student student1 = new Student("Donal Duck");
        students.add(student1);
        Student student2 = new Student("Tommy Long Surname Smith");
        students.add(student2);
        Student student3 = new Student("Tim Smith");
        students.add(student3);
        return students;
    }


    private String getExpectedOutput() {
        return "Default House : Tim Smith\n" +
                "Even House : Donal Duck\n" +
                "Long House : Tommy Long Surname Smith";
    }
}