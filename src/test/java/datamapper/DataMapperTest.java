package datamapper;

import data.HouseWithRule;
import data.Student;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataMapperTest {

    @Test
    void shouldGetHousesWithRule() {
        // given
        List<String> lines = getLinesWithHousesAndRulesData();
        // when
        List<HouseWithRule> housesWithRule = DataMapper.getHousesWithRule(lines);
        // then
        assertEquals(4, housesWithRule.size());
        assertEquals("Vowel House", housesWithRule.get(0).getHouseName());
        assertEquals("vowel", housesWithRule.get(0).getRule());
    }

    @Test
    void shouldGetStudents() {
        // given
        List<String> lines = getLinesWithStudentsData();
        // when
        List<Student> students = DataMapper.getStudents(lines);
        // then
        assertEquals(2, students.size());
        assertEquals("Donald Duck", students.get(0).getFullName());
    }

    @Test
    void shouldGetSimpleHousesWithRule() {
        // given
        Map<HouseWithRule, List<Student>> housesWithStudents = getHousesWithStudents();
        // when
        Map<String, List<String>> simpleHousesWithStudents = DataMapper.getSimpleHousesWithStudents(housesWithStudents);
        // then
        assertEquals(2, simpleHousesWithStudents.size());
        assertEquals(1, simpleHousesWithStudents.get("Long House").size());
        assertEquals("Donald Duck", simpleHousesWithStudents.get("Long House").get(0));
    }

    private List<String> getLinesWithHousesAndRulesData() {
        List<String> lines = new ArrayList<>();
        lines.add("Vowel House: vowel");
        lines.add("Long House: length");
        lines.add("Even House: even");
        lines.add("Default House: Default");
        return lines;
    }

    private List<String> getLinesWithStudentsData() {
        List<String> lines = new ArrayList<>();
        lines.add("Donald Duck");
        lines.add("Lucky Luke");
        return lines;
    }

    private Map<HouseWithRule, List<Student>> getHousesWithStudents() {
        Map<HouseWithRule, List<Student>> housesWithStudents = new HashMap<>();
        HouseWithRule house1 = new HouseWithRule("Long House", "length");
        housesWithStudents.put(house1, List.of(new Student("Donald Duck")));
        HouseWithRule house2 = new HouseWithRule("Default House", "default");
        housesWithStudents.put(house2, List.of(new Student("Lucky Luke"), new Student("Lucky Luke")));
        return housesWithStudents;
    }
}