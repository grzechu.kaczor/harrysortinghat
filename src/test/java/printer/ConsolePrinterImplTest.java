package printer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConsolePrinterImplTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private final Printer printer = new ConsolePrinterImpl();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void shouldPrintElementsInOneLine() {
        // given
        List<String> elements = getElementsToPrint();
        String expect = "Donal Duck | Mickey Mouse | Lucky Luke";
        // when
        printer.print(elements);
        // then
        assertEquals(expect, outputStreamCaptor.toString().trim());
    }

    @Test
    void shouldPrintStudentsByHouses() {
        // given
        Map<String, List<String>> housesWithStudents = getStudentsInHouses();
        String expect = getExpectedOutput();
        // when
        printer.print(housesWithStudents);
        // then
        assertEquals(expect, outputStreamCaptor.toString().trim());
    }

    private List<String> getElementsToPrint() {
        return List.of("Donal Duck", "Mickey Mouse", "Lucky Luke");
    }

    private String getExpectedOutput() {
        return "White House : Donal Duck, Mickey Mouse, Lucky Luke\n" +
                "Black House : Donal Duck, Mickey Mouse, Lucky Luke";
    }

    private Map<String, List<String>> getStudentsInHouses(){
        String student1 = "Donal Duck";
        String student2 = "Mickey Mouse";
        String student3 = "Lucky Luke";
        String house1 = "Black House";
        String house2 = "White House";
        Map<String, List<String>> housesWithStudents = new HashMap<>();
        housesWithStudents.put(house1, List.of(student1, student2, student3));
        housesWithStudents.put(house2, List.of(student1, student2, student3));
        return housesWithStudents;
    }
}